import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { CircuitosModule } from './circuitos/circuitos.module';
import { ContactoModule } from './contacto/contacto.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    CircuitosModule,
    ContactoModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
