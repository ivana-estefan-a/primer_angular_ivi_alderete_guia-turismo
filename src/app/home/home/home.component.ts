import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { listaTrekking } from 'src/app/datos/datos';
import { Trekking } from 'src/app/models/treking';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

listaTrekking: Trekking []=  listaTrekking;


constructor(private route: Router){

}

ngOnInit(): void{
listaTrekking;
}
verDetalle( trekking:Trekking): void{
this.route.navigate (['detalles'])
 
}


}