import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { CircuitosComponent } from '../circuitos/circuitos/circuitos.component';
import { CircuitosModule } from '../circuitos/circuitos.module';
import { DetallesModule } from '../detalles/detalles.module';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CircuitosModule,
    DetallesModule

  ],
  
  exports: [
    HomeComponent
  
   ]
})
export class HomeModule { }
