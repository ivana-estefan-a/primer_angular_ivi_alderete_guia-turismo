import { Component, OnInit, importProvidersFrom } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CircuitosModule } from 'src/app/circuitos/circuitos.module';
import { listaTrekking } from 'src/app/datos/datos';
import { Trekking } from 'src/app/models/treking';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.css']
})
export class DetallesComponent implements OnInit{
  listaTrekking: Trekking []=  listaTrekking;
  t: Trekking | undefined;

constructor( private activatedRoute : ActivatedRoute){}

idTrekking : number | undefined;


ngOnInit() : void {

  this.activatedRoute.paramMap.subscribe(params => {
    this.idTrekking = +params.get('id');
    console.log(this.idTrekking)
    this.t = this.listaTrekking.find(trekking=> trekking.id === this.idTrekking);
  })
}

 

}
