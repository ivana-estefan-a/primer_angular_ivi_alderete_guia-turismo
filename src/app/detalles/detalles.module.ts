import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetallesComponent } from './detalles/detalles.component';
import { SharedModule } from "../shared/shared.module";
import { CircuitosComponent } from '../circuitos/circuitos/circuitos.component';
import { CircuitosModule } from '../circuitos/circuitos.module';



@NgModule({
    declarations: [
        DetallesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        CircuitosModule
    ]
})
export class DetallesModule { }
