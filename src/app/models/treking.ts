export class Trekking {
    id: number | undefined;
    titulo: string | undefined;
    descripcion: string | undefined;
    dificultad: string | undefined;
    precio:number | undefined;
    imagen: string | undefined;
}
