import { Trekking } from "../models/treking";
export const listaTrekking: Trekking [] = [
    {id:1, titulo: "Inca Cueva", descripcion: "Sitio arquelógico, pinturas rupestres, oasis en la quebrada", dificultad: "alta", precio:5000, imagen:"3.png"},
    {id:2, titulo: "Cuevas del Huayra", descripcion: "Formaciones rocosas impresionantes. Increíbles fotografías", dificultad: "media", precio:5000, imagen:"5.png"},
    {id:3, titulo: "Castillos de Wichayra", descripcion: "Formaciones rocosas, color claro. Increíbles imágenes", dificultad: "alta", precio:10000, imagen:"7.png"},
    {id:4, titulo: "Quebrada Las Señoritas", descripcion: "Quebrada colores rojizos", dificultad: "alta", precio:15000, imagen:"9.png"},
    {id:5, titulo: "Salinas y Hornocal", descripcion: "Paisajes conocidos internacionalmente. Guiado de ensueño a las maravillas de la quebrada", dificultad: "baja", precio:12000, imagen:"12.png"}



]