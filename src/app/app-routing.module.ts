import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CircuitosComponent } from './circuitos/circuitos/circuitos.component';
import { ContactoComponent } from './contacto/contacto/contacto.component';
import { HomeComponent } from './home/home/home.component';
import { DetallesComponent } from './detalles/detalles/detalles.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent },
  {path : 'circuito', component : CircuitosComponent}, 
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path : 'contacto', component : ContactoComponent}, 
  {path : 'detalles', component : DetallesComponent} ,
  {path : 'detalles/id', component : DetallesComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
