import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CircuitosComponent } from './circuitos/circuitos.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';



@NgModule({
  declarations: [
    CircuitosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule 
  ]
})
export class CircuitosModule { }
